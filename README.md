Gestionare cupoane de reduceri (integrare cu foursquare)

Aplicatia permite logarea utilizatorului si cautarea de locatii, pentru care gaseste cupoane de reduceri valide.
In pagina principala a aplicatiei, utilizatorul va putea cauta locatii folosind functia de Cautare. Acest lucru se realizeaza 
utilizand Foursquare api. Vom face o cerere HTTP, care returneaza obiect de tip JSON.
Din toolbar-ul paginii principale, putem da click pe un buton care va duce catre fereastra de filtrare a rezultatelor(locatiilor) 
din tara/orasul ales. Aici avem posibilitatea filtrarii cupoanelor de reducere 
pe categoria dorita: Restaurante/cafenele, Magazine, Alte Activitati. Datele utilizatorului (nume, telefon/mail) si ale 
locatiei alese (tara, oras) vor fi salvate intr-o baza de date.
Tot din pagina principala, putem accesa pagina cu cupoanele disponibile si detaliile despre locatia aleasa de noi (adresa, 
program de functionare). 
In dezvoltarea aplicatiei, este necesar sa ne facem cont pe foursquare pt a primi API key. Vom primi ID Client si Server Client,
cu care realizam cererea catre api.
Aplicatia contine toate functinalitatile relevante, pagina principala contine buton de logare si buton de creare cont. 
Dupa logare, se intra pe profilul de utilizator
unde se gasesc: intr-o parte, ofertele populare din orasul selectat cand s-a facut contul, iar mai jos, 
bara de search pentru cautarea unei anumite locatii.
In pagina de filtrare avem butoanele sorteaza si categorii, cu care putem sorta cupoanele in functie de pret, alfabetic,
dar si in categorii precum restaurante/ cafenele, magazine, activitati, muzee etc.
Fiecare cupon poate fi adaugat in categoria "Favorite". Pentru cupoanele din aceasta categorie, aplicatia trimite 
o notificare ( alerta push sau via e-mail, in functie de preferintele clientului) care actioneaza cand urmeaza sa expire oferta.
Documentare API:
Foursquare api ne ofera acces catre o baza de date foarte mare cu locatii din toata lumea, acestea putand fi filtrate in functie de 
anumite criterii precum: adresa, popularitate, sfaturi utile, poze etc.


    






